# StepKit

Started by Fran Fernandez as SwiftNUStep for importing Foundation, StepKit allows for the creation of ctoss-platform (Linux or macOS) Cocoa applications using GNUStep.

## Modules

Each module uses the OS prefix, meaning OpenStep.

- Foundation: OSKit
- AppKit: OSAppKit